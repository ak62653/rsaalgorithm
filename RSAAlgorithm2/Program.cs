﻿using System;
using System.IO;
using System.Numerics;
using System.Text;

class RSADemo
{
    static void Main()
    {
        Console.WriteLine("The RSA Algorithm Encryption and Decryption!");

        
        Console.WriteLine("Enter prime number p:");
        BigInteger p = BigInteger.Parse(Console.ReadLine());

        Console.WriteLine("Enter prime number q:");
        BigInteger q = BigInteger.Parse(Console.ReadLine());

       
        BigInteger n = p * q;
        BigInteger phi = (p - 1) * (q - 1);

        
        BigInteger e = 65537;

       
        BigInteger d = ModInverse(e, phi);

       
        Console.WriteLine("Enter plaintext message:");
        string plaintext = Console.ReadLine();

        
        BigInteger[] plaintextInt = StringToNumericValue(plaintext);

       
        BigInteger[] encryptedInt = new BigInteger[plaintextInt.Length];
        for (int i = 0; i < plaintextInt.Length; i++)
        {
            encryptedInt[i] = BigInteger.ModPow(plaintextInt[i], e, n);
        }

        
        SaveToFile("ciphertext.txt", string.Join(",", encryptedInt));
        SaveToFile("public_key.txt", $"{n},{e}");

        Console.WriteLine("Ciphertext and Public key are saved.");

        
        Console.WriteLine("Encrypted message: " + string.Join(",", encryptedInt));

       
        string ciphertextFromFile = ReadFromFile("ciphertext.txt");
        string[] ciphertextArray = ciphertextFromFile.Split(',');
        BigInteger[] encryptedIntFromFile = Array.ConvertAll(ciphertextArray, BigInteger.Parse);

        
        BigInteger[] decryptedInt = new BigInteger[encryptedIntFromFile.Length];
        for (int i = 0; i < encryptedIntFromFile.Length; i++)
        {
            decryptedInt[i] = BigInteger.ModPow(encryptedIntFromFile[i], d, n);
        }

        
        string decryptedText = NumericValueToString(decryptedInt);

        Console.WriteLine("Decrypted message: " + decryptedText);


        if (plaintext == decryptedText)
        {
            Console.WriteLine("Encryption and Decryption are successful.");
        }
        else
        {
            Console.WriteLine("Encryption and Decryption failed.");
        }
    }

    static BigInteger ModInverse(BigInteger a, BigInteger n)
    {
        BigInteger i = n, v = 0, d = 1;
        while (a > 0)
        {
            BigInteger t = i / a, x = a;
            a = i % x;
            i = x;
            x = d;
            d = v - t * x;
            v = x;
        }
        v %= n;
        if (v < 0) v = (v + n) % n;
        return v;
    }

    static void SaveToFile(string filename, string content)
    {
        File.WriteAllText(filename, content);
    }

    static string ReadFromFile(string filename)
    {
        return File.ReadAllText(filename);
    }

    static BigInteger[] StringToNumericValue(string input)
    {
        byte[] bytes = Encoding.ASCII.GetBytes(input);
        BigInteger[] result = new BigInteger[bytes.Length];
        for (int i = 0; i < bytes.Length; i++)
        {
            result[i] = bytes[i];
        }
        return result;
    }

    static string NumericValueToString(BigInteger[] input)
    {
        byte[] bytes = Array.ConvertAll(input, x => (byte)x);
        return Encoding.ASCII.GetString(bytes);
    }
}
